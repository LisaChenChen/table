export class User {
    name: string;
    phone: string;
    email: string;
    company: string;
    date_entry: string;
    org_num: string;
    address_1: string;
    city: string;
    zip: string;
    geo: string;
    pan: string;
    pin: string;
    id: number;
    statuc: string;
    fee: string;
    guid: string;
    date_exit: string;
    date_first: Date;
    date_recent: Date;
    url: string;
}