import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { User } from '../user';
import { HttpClient } from '@angular/common/http';
  
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass']
})
export class TableComponent implements OnInit {

  users: User[] = [];
  rows: number ;

  tablePageList = [];   
  pageNo = 1;  
  preShow = false;  
  nextShow = true;  
  pageSize = 5;  
  totalCount = 0;  
  pageSizes = [5, 10, 15]; 
  curPage = 1;  
 
  flag:boolean ;
   

  constructor(private userService: UserServiceService,private http: HttpClient) { 
    this.rows = 200;
  }

  ngOnInit() {
    this.userService.getUser().subscribe(
      (date:any) =>{
        this.users = date;
      }
    );
    this.flag = true;
     this.getPageList();
  }
  getPageList(){
    if (this.users.length >= 1) {
      if (this.users.length % this.pageSize === 0) {
        this.pageNo = Math.floor(this.users.length / this.pageSize);
      } else {
        this.pageNo = Math.floor(this.users.length / this.pageSize) + 1;
      }
      if (this.pageNo < this.curPage) {
        this.curPage = this.curPage - 1;
      }
      if (this.pageNo === 1 || this.curPage === this.pageNo) {
        this.preShow = this.curPage !== 1;
        this.nextShow = false;
      } else {
        this.preShow = this.curPage !== 1;
        this.nextShow = true;
      }
    } else {
      this.users.length = 0;
      this.pageNo = 1;
      this.curPage = 1;
    }
    this.tablePageList = this.users.slice((this.curPage - 1) * this.pageSize, (this.curPage) * this.pageSize);
 
  }
  showPrePage() {
    this.curPage--;
    if (this.curPage >= 1) {
      this.getPageList();
    } else {
      this.curPage = 1;
    }
  }
  showNextPage() {
    this.curPage++;
    if (this.curPage <= this.pageNo) {
      this.getPageList();
    } else {
      this.curPage = this.pageNo;
    }
  }
  onChangePage(value) {
    if (value > this.pageNo) {
      confirm('out of max');
    } else if (value <= 0) {
      this.curPage = 1;
      this.getPageList();
    } else {
      this.curPage = value;
      this.getPageList();
    }
  }
  onChangePageSize(value) {
    this.pageSize = value;
    this.curPage = 1;
    this.getPageList();
  }
  onEnter(value){
    this.pageSize = value;
  }
  submit(id){
    this.userService.postUserDetails(id);
    console.log(id);
  }
  ngOnDestory(){
   }
   
  

}
