import { Injectable } from '@angular/core';
import { User } from './user';
import {HttpClient, HttpResponse} from '@angular/common/http';

 
@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  Url:string = 'assets/sample.json';
  constructor(private http: HttpClient) { }
  getUser(){
    return this.http.get(this.Url);
  }

  postUserDetails(id){
    return this.http.post("", id);
  }
}
